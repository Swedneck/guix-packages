(define-module (swedneck packages marblemarcher)
  ;; all of this was missing
  #:use-module (guix packages)
  #:use-module (guix build-system cmake) ; it uses cmake
  #:use-module (guix licenses) ; this is for gpl2
  #:use-module (gnu packages)
  #:use-module (gnu packages game-development) ; for SFML
  #:use-module (gnu packages algebra) ; for Eigen
  #:use-module (gnu packages gl) ; for gl headers
  ;; and finally what was here already
  #:use-module (guix git-download))

;; NOTE: This package turned out to be a bit more involved than one would
;;       hope. You should probably go for lemonbar or similar for your
;;       first proper package as there's just enough to get your hands
;;       dirty but nowhere near the mess thins thing would be for a beginner.
;;
;;       I specifically suggested lemonbar as I know it's easy to package
;;       so even if you abandon it I see it as a good first step past packaging
;;       "hello world".
(define-public marblemarcher
  ;; the standard indentaiton might seem like a chore but it's really
  ;; great for catching errors with your parens
  (let ((commit "50192b47ed387dba5420824d17b18a8283b1f43b")
        (revision "1"))
    ;; let binding must enclose the package def or the commit and revision
    ;; variables won't be in scope
    (package
      (name "marblemarcher")
      (version (string-append "0.0.0-" revision "." (string-take commit 7)))
      (source (origin
                (method git-fetch)
                ;; keep indentation consistent regardless of the scheme you use
                (uri (git-reference
                       (url "https://github.com/HackerPoet/MarbleMarcher")
                       (commit commit)))
                (file-name (string-append name "-" version "-checkout"))
                (sha256
                  (base32
                    ;; hash was wrong but you knew that as we went the lazy
                    ;; way for guix to scream the right one back in bold red text
                    "0a1krvsw24a1cm9qzvgkz19d6k9z72b9z9f730al0rjksv1l0ad4"))))
      (build-system cmake-build-system)
      ;; We actually require the configure step for cmake but testing fails so has
      ;; been left out. (hint: there's no tests)
      (arguments
        '(#:tests? #f ; no tests apparently
          #:phases
          (modify-phases %standard-phases
            ;; we need to fix the assets path because the application assumes they're
            ;; in the current directory rather than something sane
            (add-after 'unpack 'fix-assets-location
              ;; lambda* allows for named (key) and optional arguments specified after
              ;; #:key. We use #:allow-other-keys to ignore the extra arguments passed
              ;; to this function as we're not really interested in them.
              (lambda* (#:key outputs #:allow-other-keys)
                (let* ((out (assoc-ref outputs "out"))
                       (data (string-append out "/share/marblemarcher/")))
                  ;; sed like substitution to fix asset paths to the one we set
                  (substitute* "src/Res.h"
                    (("assets/") (string-append data "assets/")))
                  #t)))
            ;; we have nothing to check...
            (delete 'check)
            ;; NOTE: remove the insult before placing in Guix proper
            ;;       as it won't be accepted even if he deserves it.
            ;;
            ;; dev doesn't provide an install step due to being incompetent
            ;; so we must do thins manually ourselves
            (replace 'install
              (lambda* (#:key inputs outputs #:allow-other-keys)
                ;; let binding here keeps things tidy
                (let* ((out (assoc-ref outputs "out"))
                       ;; we want both the output location and source
                       ;; in scope as the binary ends up in the current
                       ;; working directory while assets are left behind
                       ;; in the source directory
                       (inp (assoc-ref inputs  "source"))
                       ;; set the same path (I should have set this outside
                       ;; modify-phases but oh well)
                       (data (string-append out "/share/marblemarcher/"))
                       ;; output location (only contents will be the game
                       ;; where this is later symlinked to your user profile
                       ;; when installed)
                       (bin (string-append out "/bin/")))
                  (install-file "MarbleMarcher" bin)
                  ;; need to create directories or else it will complain
                  (mkdir-p data)
                  ;; copy assets to the path we set so the game can find them
                  (copy-recursively
                    (string-append inp "/assets")
                    (string-append data "assets"))))))))

      ;; you never listed any dependencies so this package would have failed to build
      ;; when you tried installing it even when syntax issues were resolved
      (inputs
        `(("sfml" ,sfml)
          ("mesa" ,mesa) ; this isn't listed by the author but needed to build...
          ("eigen" ,eigen)))
      ;; you should actually make this fit for 80 char terminals
      (synopsis "Marble Marcher is a video game demo that uses a fractal physics
engine and fully procedural rendering to produce beautiful and unique gameplay
unlike anything you've seen before.")
      (description "Marble Marcher is a video game demo that uses a fractal
physics engine and fully procedural rendering to produce beautiful and
unique gameplay unlike anything you've seen before.

The goal of the game is to reach the flag as quickly as possible. But be
careful not to fall off the level or get crushed by the fractal! There are 24
levels to unlock.")
      (home-page "https://codeparade.itch.io/marblemarcher")
      ;; license gpl-2.0 was wrong
      (license gpl2))))

;; uncomment for easier testing with `guix environment -l marblemarcher.scm`
;; and/or `guix build -f ./marblemarcher.scm` since we directly return the
;; package object for guix to pick up.
; marblemarcher

;; NOTE: this should go in gnu/packages/games.scm when moved to guix proper
;;       as with the other games
